# This is my .tmux config for remote hosts I only SSH into.

# Server options
set -s default-terminal "xterm-256color"
set -s history-file "$HOME/.tmux_history"

# Global session options
set -g prefix C-o
set -g set-titles on
set -g history-limit 4096
set -g status on
set -g display-time 500
set -g mouse on
set -g visual-activity off
set -g visual-bell off
set -g status-right-length 80
set -ag update-environment "SSH_TTY"

# Global window options
setw -g monitor-activity off
setw -g monitor-bell off

# Binds
bind -T prefix C-o last-window
bind -T prefix o send-prefix
bind -T prefix '"' choose-window
bind -T prefix A command-prompt -I "#W" "rename-window -- %%"
bind -T prefix k confirm-before -p "kill-window #W? (y/n)" kill-window
bind -T prefix 2 split-window
bind -T prefix 3 split-window -h
bind -T prefix C-p select-pane -U
bind -T prefix C-n select-pane -D
bind -T prefix C-b select-pane -L
bind -T prefix C-f select-pane -R
bind -T prefix M setw monitor-activity on
bind -T prefix m setw monitor-activity off
bind -T prefix r source-file "$HOME/.tmux.conf" \; display "Reloaded."
bind -T prefix R move-window -r
bind -T prefix T command-prompt -p theme: -I "#W" "source $THEMES_DIR/%%.tmuxtheme"
bind -T copy-mode M-w send-keys -X copy-pipe-and-cancel "$HOME/scripts/pbcopy.sh"
bind -T copy-mode C-w send-keys -X copy-pipe-and-cancel "$HOME/scripts/pbcopy.sh"
bind -T copy-mode M-p send -X page-up
bind -T copy-mode M-n send -X page-down
bind -T copy-mode C-g send -X cancel

unbind -T copy-mode MouseDragEnd1Pane

# Based on Powerline Orange Theme by Jim Myhrberg <contact@jimeh.me>
set -g status-style fg=colour240,bg=colour233
set -g status-left-style bg=colour233,fg=colour243
set -g status-left-length 40
set -g status-left "#[fg=colour232,bg=colour130,bold] #S #[fg=colour235,bg=colour233,nobold]"
set -g status-right-style bg=colour233,fg=colour243
set -g status-right-length 0
set -g status-right ""
set -g window-status-format " #I:#W#F "
set -g window-status-current-format " #I:#W#F "
set -g window-status-current-style bg=colour130,fg=colour232
set -g window-status-activity-style bg=colour233,fg=colour130
set -g window-status-separator ""
set -g status-justify centre
set -g pane-border-style bg=default,fg=colour238
set -g pane-active-border-style bg=default,fg=colour130
set -g display-panes-colour colour233
set -g display-panes-active-colour colour245
set -g message-style bg=colour130,fg=black
set -g message-command-style bg=colour233,fg=black
set -g mode-style bg=colour130,fg=colour232
