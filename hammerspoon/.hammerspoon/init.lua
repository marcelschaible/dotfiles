-- Some global variables
local tmux = "/usr/local/bin/tmux"
local hyper = {"cmd", "alt", "ctrl", "shift"}
local super = {"cmd", "alt", "ctrl"}
local hyperFuncs = {}
local alternates = {}
local superFuncs = {}
local DEBUG = true

local function debug(fmt, ...)
    if not DEBUG then
	return
    end
    print(string.format(fmt, ...))
end

local function getAppMainWindow(app)
    local myapp = hs.appfinder.appFromName(app)
    if not myapp then
	return nil
    end
    local mainwin = myapp:mainWindow()
    if not mainwin then
	return nil
    end
    return mainwin
end

-- Plays an array of keystroke events.
local function playKb(kbd)
    for _,v in pairs(kbd) do
	if #v == 2 then
	    hs.eventtap.keyStroke(v[1], v[2], 10000)
	elseif #v == 1 then
	    hs.eventtap.keyStrokes(v[1])
	end
    end
end

local function unhideApp(app)
    debug("unhideApp: %s", app)
    local myapp = hs.appfinder.appFromName(app)
    if not myapp then
	debug("unhideApp: Can't find '%s', bailing out.", app)
	return nil
    end
    debug("unhideApp: Got app: %s", myapp)
    if not myapp:isRunning() then
	debug("unhideApp: '%s' is not running, bailing out.", app)
	return nil
    end
    -- Unhide the app if it's hidden
    if myapp:isHidden() then
	debug("unhideApp: '%s' is hidden, unhiding..", app)
	myapp:unhide()
    end
    -- If the app has a minimized window, unminimize it
    for _,v in pairs(myapp:allWindows()) do
	if v:isMinimized() then
	    debug("unhideApp: '%s' window '%s' is minimized.", app, v)
	    v:unminimize()
	end
    end
    if not myapp:isFrontmost() then
	debug("unhideApp: '%s' is not frontmost. Fixing.", app)
	if not myapp:activate(true) then
	    debug("unhideApp '%s' activation failed", app)
	end
    end
    debug("unhideApp: '%s' done.", app)
end

local function focusMainWindow(mainwin)
    -- local app = mainwin:application()
    debug("focusMainWindow: '%s'", mainwin)
    mainwin:application():activate(true)
    mainwin:application():unhide()
    mainwin:focus()
end

local function runCommand(command)
    local handle = io.popen(command)
    if handle == nil then
	return nil, {false}
    end
    local out = handle:read("*a")
    local rc = {handle:close()}
    return out, rc
end

local function emacsAgenda()
    local ec = "/usr/local/bin/emacsclient -e '(yequake-toggle \"agenda\")'"
    return runCommand(ec)
end

local function emacsFrame(file)
    -- local ec = "/usr/local/bin/emacsclient -n -c --frame-parameters=\"((undecorated . t))\" %s"
    local ec = "/usr/local/bin/emacsclient -n -c -F \"((window-system . ns))\""
    if file ~= nil then
	ec = ec .. " " .. file
    end
    local cmd = string.format(ec, file)
    local ret = runCommand(cmd)
    return ret
end

-- Bring focus to the specified app, maybe launch it.
-- Maybe bring up an alternate and focus or launch it.
local function toggleApp(appinfo)
    -- debug("toggleApp: '%s'", appinfo.name)
    local alternate = alternates[appinfo.name]
    local app = hs.application.find(appinfo.name)
    if not app then
	-- App isn't running.
	debug("toggleApp: '%s' is not running.", appinfo.name)
	if appinfo.launch or appinfo.alwayslaunch then
	    debug("toggleApp: launching '%s'", appinfo.name)
	    if (appinfo.name == "iTerm2") then
		-- naming is weird here.
		hs.application.launchOrFocus("/Applications/iTerm.app")
	    else
		if not hs.application.launchOrFocus(appinfo.name) then
		    -- maybe it's a bundle id?
		    hs.application.launchOrFocusByBundleID(appinfo.name)
		end
	    end
	    if appinfo.rect ~= nil or appinfo.kbd ~= nil then
		-- allow time app to start
		hs.timer.usleep(2000000)
	    end
	    if appinfo.rect ~= nil then
		local win = hs.window.focusedWindow()
		if win ~= nil then
		    win:setFrame(appinfo.rect)
		end
	    end
	    if appinfo.kbd ~= nil then
		playKb(appinfo.kbd)
	    end
	else
	    if alternate ~= nil then
		toggleApp(alternate)
	    end
	end
        return
    end
    -- App is running, let's focus it.
    local mainwin = app:mainWindow()
    if mainwin then
	debug("toggleApp: '%s' has a main window.", appinfo.name)
        if mainwin ~= hs.window.focusedWindow() then
	    focusMainWindow(mainwin)
        else
	    -- App already has the focus. Let's raise or launch the alternate.
	    if alternate ~= nil then
		toggleApp(alternate)
	    end
	end
	return
    end
    debug("toggleApp: '%s' does NOT have a main window.", appinfo.name)
    if (appinfo.name == 'Finder') then
	-- Finder is a special case, it's running, but needs a new window.
	hs.appfinder.appFromName(appinfo.name):activate()
	if appinfo.rect ~= nil or appinfo.kbd ~= nil then
	    -- allow time for the switch
	    hs.timer.usleep(1000000/4)
	end
	if appinfo.kbd ~= nil then
	    playKb(appinfo.kbd)
	end
	return
    end
    if (appinfo.name == 'org.gnu.Emacs') then
	-- ok so no emacs main window
	local count = 0
	for _ in pairs(app:allWindows()) do count = count + 1 end
	if count == 0 then
	    emacsFrame(nil)
	else
	    debug("Emacs has %d windows.", count)
	end
    end
    -- it's probably a minimized or otherwise hidden app
    unhideApp(appinfo.name)
    if appinfo.alwayslaunch then
	debug("'%s': always launch is set, so launching.", appinfo.name)
	-- This is usually for 1Password, which is running but needs
	-- to be re-launched to make it visible.
	if not hs.application.launchOrFocus(appinfo.name) then
	    hs.application.launchOrFocusByBundleID(appinfo.name)
	end
    end
end

local function gotoTmuxWindow(window)
    local cmd = tmux .. " select-window -t '" .. window .. "'"
    local out, rc =  runCommand(cmd)
    out = out:gsub("%s+$", "")
    return out, rc
end

local function tmuxGotoHost(hostname)
    local local_tmux = tmux .. " new-window -n " .. hostname
    local remote_tmux = "tmux -u new -A -D -s " .. hostname
    local ssh_cmd = "ssh -tt -q " .. hostname .. " " .. remote_tmux
    local cmd = local_tmux .. " " .. ssh_cmd
    return runCommand(cmd)
end

local function tmuxWindowExists(win)
    local out, rc = runCommand(tmux .. " list-windows")
    if not rc[1] then
	return false
    end
    for line in out:gmatch("[^\r\n]+") do
	if string.find(line, win) ~= nil then
	    return true
	end
    end
    return false
end

local function gotoHost(hostname)
    if not tmuxWindowExists(hostname) then
	tmuxGotoHost(hostname)
    end
    gotoTmuxWindow(hostname)
    local termMainWindow = getAppMainWindow("iTerm2")
    if termMainWindow == nil then
	return
    end
    focusMainWindow(termMainWindow)
end

local function applyLayout()
    debug("applyLayout()")
    local main = "LG HDR 4K"
    local builtin = "Color LCD"

    for k,v in pairs(hs.screen.allScreens()) do
	debug(string.format("Screen %d = %s", k, v:name()))
    end

    local windowLayout = {
	{"Safari",        nil, main,    hs.layout.left50,    nil, nil},
	{"Firefox",       nil, main,    hs.layout.left50,    nil, nil},
	{"Google Chrome", nil, main,    hs.layout.left50,    nil, nil},
	{"iTerm2",        nil, main,    hs.layout.right50,   nil, nil},
	{"org.gnu.Emacs", nil, main,    hs.layout.right50,   nil, nil},
        {"Messages",      nil, builtin, hs.layout.maximized, nil, nil},
        {"iTunes",        nil, builtin, hs.layout.maximized, nil, nil},
        {"Slack",         nil, builtin, hs.layout.maximized, nil, nil},
    }
    -- Let's unhide all the apps, in case they're hidden.
    for _,v in pairs(windowLayout) do
	unhideApp(v[1])
    end
    hs.layout.apply(windowLayout)
end

local function applyAltLayout()
    debug("applyAltLayout()")
    local main = "LG HDR 4K"

    for k,v in pairs(hs.screen.allScreens()) do
	debug(string.format("Screen %d = %s", k, v:name()))
    end

    local erect = hs.geometry.rect(954, 0, 943, 1076)
    local frect = hs.geometry.rect(0, 0, 950, 1080)

    local windowLayout = {
        {"Firefox",       nil, main, nil, nil, frect},
        {"org.gnu.Emacs", nil, main, nil, nil, erect},
        {"iTerm2",        nil, main, hs.layout.left50, nil, nil},
    }
    -- Let's unhide all the apps, in case they're hidden.
    for _,v in pairs(windowLayout) do
	unhideApp(v[1])
    end
    hs.layout.apply(windowLayout)
end


-- Reload config
local function reloadConfig(paths)
    local doReload = false
    for _,file in pairs(paths) do
	if file:sub(-9)  == "/init.lua" then
            debug("File: %s changed, reloading", file)
            doReload = true
        end
    end
    if not doReload then
        return
    end
    hs.reload()
end

local function toggleDarkMode()
    local script = [[
    tell application "System Events"
        tell appearance preferences
            set dark mode to not dark mode
        end tell
    end tell
    ]]
    hs.applescript(script)
end

local function printWindowID()
    local window = hs.window.focusedWindow()
    print(window:id())
end

local function printWindowLayout()
    local window = hs.window.focusedWindow()
    local f = window:frame()
    print(string.format("{x=%d,y=%d,h=%d,w=%d}", f.x, f.y, f.h, f.w))
end

local function toggleInputDevice()
    local builtInMic = "Podcast Input"
    local fancyMic = "Universal Audio Thunderbolt"
    local curInput = hs.audiodevice.defaultInputDevice()

    if curInput:name() == builtInMic then
	local fancy = hs.audiodevice.findDeviceByName(fancyMic)
	if fancy ~= nil then
	    hs.alert.show("Current mic: " .. fancyMic)
	    fancy:setDefaultInputDevice()
	end
    elseif curInput:name() == fancyMic then
	local builtIn = hs.audiodevice.findDeviceByName(builtInMic)
	if builtIn ~= nil then
	    hs.alert.show("Current mic: " .. builtInMic)
	    builtIn:setDefaultInputDevice()
	end
    end
end

local function changeVolume(diff)
  return function()
    local current = hs.audiodevice.defaultOutputDevice():volume()
    local new = math.min(100, math.max(0, math.floor(current + diff)))
    if new > 0 then
      hs.audiodevice.defaultOutputDevice():setMuted(false)
    end
    hs.alert.closeAll(0.0)
    hs.alert.show("Volume " .. new .. "%", {}, 0.5)
    hs.audiodevice.defaultOutputDevice():setVolume(new)
  end
end

local configFileWatcher = hs.pathwatcher.new(os.getenv("HOME") .. "/.hammerspoon/", reloadConfig)
configFileWatcher:start()

local fkb = {
    {{"cmd"}, "n"}
}

local zrt = {x=415,y=201,h=660,w=880}    -- Zoom Rect

-- Application switch & launch hotkeys.
hyperFuncs['t'] = function() toggleApp({name="iTerm2",             launch=true,       kbd=nil, rect=nil}) end
hyperFuncs['w'] = function() toggleApp({name="Firefox",            launch=true,       kbd=nil, rect=nil}) end
hyperFuncs['c'] = function() toggleApp({name="Messages",           launch=true,       kbd=nil, rect=nil}) end
hyperFuncs['z'] = function() toggleApp({name="Zoom",               launch=true,       kbd=nil, rect=zrt}) end
hyperFuncs['f'] = function() toggleApp({name="Finder",             launch=true,       kbd=fkb, rect=nil}) end
hyperFuncs['1'] = function() toggleApp({name="1Password 7",        alwayslaunch=true, kbd=nil, rect=nil}) end
hyperFuncs['p'] = function() toggleApp({name="System Preferences", launch=true,       kbd=nil, rect=nil}) end
hyperFuncs['e'] = function() toggleApp({name="org.gnu.Emacs",      launch=false,      kbd=nil, rect=nil}) end
hyperFuncs['i'] = function() toggleApp({name="iTunes",             launch=true,       kbd=nil, rect=nil}) end
hyperFuncs['l'] = function() applyAltLayout() end

-- Super hotkeys.
superFuncs['a'] = function() emacsAgenda() end
superFuncs['c'] = function() emacsFrame("~/.emacs.d/init.el") end
superFuncs['o'] = function() emacsFrame("~/org") end
superFuncs['d'] = function() toggleDarkMode() end
superFuncs['e'] = function() gotoHost("eidolon") end
superFuncs['i'] = function() printWindowID() end
superFuncs['k'] = function() runCommand("~/bin/controlkill.sh") end
superFuncs['l'] = function() applyLayout() end
superFuncs['m'] = function() toggleInputDevice() end
superFuncs['p'] = function() printWindowLayout() end

-- Do the binding.
for _hotkey, _fn in pairs(hyperFuncs) do
    hs.hotkey.bind(hyper, _hotkey, _fn)
end

for _hotkey, _fn in pairs(superFuncs) do
    hs.hotkey.bind(super, _hotkey, _fn)
end

-- Some manual binds
hs.hotkey.bind(super, 'UP',    hs.itunes.play)
hs.hotkey.bind(super, 'DOWN',  hs.itunes.pause)
hs.hotkey.bind(super, 'LEFT',  hs.itunes.previous)
hs.hotkey.bind(super, 'RIGHT', hs.itunes.next)

-- Volume keys
hs.hotkey.bind(hyper, 'UP',    changeVolume(3))
hs.hotkey.bind(hyper, 'DOWN',  changeVolume(-3))

-- hs.hotkey.bind({"alt"}, "return", function() editWithEmacs() end)
