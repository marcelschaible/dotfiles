#!/bin/bash
# This script assumes you're SSH'd into this box from tmux on another box.

buf=$(cat "$@")
buflen=$(printf %s "$buf" | wc -c)
maxlen=74994
if [ "$buflen" -gt "$maxlen" ]; then
   printf "input is %d bytes too long" "$(( buflen - maxlen ))" >&2
fi

# Wrap the ANSI OSC 52 message in a tmux DCP (device control string)
# This tells tmux running on your laptop to pass the OSC 52 up.
esc="\033]52;c;$( printf %s "$buf" | head -c $maxlen | base64 | tr -d '\r\n' )\a"
esc="\033Ptmux;\033$esc\033\\"

# We running tmux inside tmux?
if [ ! -z "${TMUX+foo}" ]; then
    # Now we need to bypass the local tmux when we send this, so send it to
    # tmux specific SSH_TTY. We can't use the generic SSH_TTY var because we
    # might have attached and re-attached, so that variable would be stale.
    #
    # Put this in your .tmux.conf to make this work:
    # set -g update-environment "SSH_TTY"
    #
    OUTPUT_TTY="$(tmux show-environment SSH_TTY | cut -d'=' -f2)"
else
    OUTPUT_TTY=/dev/stdout
fi
printf "$esc" > "$OUTPUT_TTY"
