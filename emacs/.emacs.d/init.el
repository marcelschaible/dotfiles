(setq user-full-name "Mike Hamrick")
(setq user-mail-address "mikeh@muppetlabs.com")

(setq gc-cons-threshold (* 20 1024 1024))
(defconst custom-file (expand-file-name "custom.el" user-emacs-directory))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))
(load custom-file)

; Let's not exit emacs. Deleting the frame is almost always what I want.
; This is because I run emacs in server mode all the time.
(global-set-key (kbd "C-x C-c") 'delete-frame)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))
(require 'package)
(package-initialize)
(setq package-enable-at-startup nil)
(setq package-archives ())
(add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/") t)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(defvar my-auto-save-dir
	(file-name-as-directory (expand-file-name
				 "autosave" user-emacs-directory)))
(unless (file-exists-p my-auto-save-dir)
  (make-directory my-auto-save-dir))

(add-to-list 'auto-save-file-name-transforms
             (list "\\(.+/\\)*\\(.*?\\)" (expand-file-name "\\2" my-auto-save-dir))
             t)

(require 'use-package-ensure)
(setq use-package-always-ensure t)
(setq backup-by-copying t
      backup-directory-alist
      `((".*" . ,(expand-file-name "backups" user-emacs-directory)))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t)

(unless window-system
  (xterm-mouse-mode)
  (global-set-key (kbd "<mouse-4>") 'scroll-down-line)
  (global-set-key (kbd "<mouse-5>") 'scroll-up-line))

; UTF-8 all the things!
(set-charset-priority 'unicode)
(setq locale-coding-system   'utf-8)
(set-terminal-coding-system  'utf-8)
(set-keyboard-coding-system  'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system        'utf-8)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

(setq inhibit-splash-screen t)
(setq inhibit-startup-screen t)
(setq inhibit-startup-buffer-menu t)
(setq inhibit-startup-message t)
(setq inhibit-startup-echo-area-message t)
(setq ring-bell-function 'ignore)
(setq create-lockfiles nil)
(setq vc-follow-symlinks t)
(setq message-log-max 10000)
(fset 'yes-or-no-p 'y-or-n-p)

(menu-bar-mode 0)
(scroll-bar-mode 0)
(tool-bar-mode 0)
(save-place-mode 1)
(set-fill-column 80)

(require 'epa)
(setq epa-pinentry-mode 'loopback)

; (add-to-list 'default-frame-alist '(top . 0))
; (add-to-list 'default-frame-alist '(left . 960))
; (add-to-list 'default-frame-alist '(width . 0.5))
; (add-to-list 'default-frame-alist '(height . 1.0))
; (add-to-list 'default-frame-alist '(undecorated . t))
; (add-to-list 'default-frame-alist '(ns-transparent-titlebar . t))
; (add-to-list 'default-frame-alist '(ns-appearance . dark))
(add-to-list 'default-frame-alist '(font . "InconsolataGo NF-22"))

(defun my-start-frame (frame)
  "My frame startup."
  (interactive)
  (with-selected-frame frame
    (when (daemonp)
      (menu-bar-mode 0)
      (scroll-bar-mode 0)
     ;(set-face-attribute 'fringe nil :background nil)
      (when (memq window-system '(mac ns))
        (setq frame-resize-pixelwise t)
        (set-frame-position (selected-frame) 954 0)
        (set-frame-size (selected-frame) 943 1076 t)
        (setq insert-directory-program "/usr/local/bin/gls")
	(message "Your macOS flavor is: %s!" window-system)
	(use-package exec-path-from-shell
	  :config
	  (exec-path-from-shell-initialize))
	(setq mac-option-modifier 'meta)
	(global-set-key [s-return] (quote toggle-frame-maximized))
	(setq ns-function-modifier 'super))
      (message "Frame start complete."))))

(add-to-list 'after-make-frame-functions #'my-start-frame)

(use-package diminish)
(use-package delight)

(use-package smart-mode-line
  :init
  (setq sml/no-confirm-load-theme t)
  (add-hook 'after-init-hook 'sml/setup))
(use-package smart-mode-line-atom-one-dark-theme)
(load-theme `smart-mode-line-atom-one-dark t)

(use-package zenburn-theme
  :custom
  (zenburn-override-colors-alist '(("zenburn-bg"    . "#212121")
				   ("zenburn-bg-1"  . "#111111")
				   ("zenburn-bg-2"  . "#000000")
				   ("zenburn-bg+1"  . "#313131")
				   ("zenburn-bg+2"  . "#414141")
				   ("zenburn-bg+3"  . "#515151")))
  (zenburn-use-variable-pitch t)
  (zenburn-scale-org-headlines t)
  (zenburn-scale-outline-headlines t))
(load-theme `zenburn t)

(use-package tramp
  :commands tramp-set-completion-function
  :config
  (setq tramp-default-method "ssh")
  (setq tramp-use-ssh-controlmaster-options nil)
  (tramp-set-completion-function
   "ssh"
   '((tramp-parse-sconfig "/etc/ssh_config")
     (tramp-parse-sconfig "~/.ssh/config"))))

(use-package edit-server
  :if (daemonp)
  :init
  (add-hook 'after-init-hook 'edit-server-start t))

(use-package iedit
  :bind ("C-\\" . iedit-mode))

(use-package indent-tools
  :init
  (global-set-key (kbd "C-c >") 'indent-tools-hydra/body))

(use-package org-bullets
  :init
  (add-hook 'org-mode-hook #'org-bullets-mode))

(use-package ox-gfm)
(use-package htmlize)
(require 'org-mouse)

(use-package org
  :commands org-indent-mode
  :config
  (defvar my-org-dir "/Users/mikeh/org")
  (defvar my-org-publish-dir "/Users/mikeh/git/public")
  (defun my-maybe-lob-ingest ()
    (if (and buffer-file-name
             (string-match
              (format "%s/.*code\\.inc$" my-org-dir)
              buffer-file-name))
        (org-babel-lob-ingest buffer-file-name)))
  (defun my-after-save-hook ()
    (my-maybe-lob-ingest))
  (defun my-org-mode-hook ()
    (my-maybe-lob-ingest)
    (turn-on-auto-fill)
    (turn-on-flyspell)
    (org-indent-mode 1)
    (setq fill-column 80))
  (defun my-firefox (ppl)
    (start-process "fox" nil "open" "-a"
                   "firefox" (format "file://%s" my-org-publish-dir)))
  (defun my-git-publish (ppl)
    (let ((publish-script (format "%s/publish.sh" my-org-publish-dir)))
      (when (file-executable-p publish-script)
          (start-process-shell-command "pub" nil publish-script))))
  (defun my-org-confirm-babel-evaluate (lang body)
    (not (member lang '("sh" "python" "elisp" "ruby" "shell" "dot"
                        "perl" "jupyter-python" "jupyter-ruby"
                        "jupyter-perl" "jupyter-lua"))))
  (defun my-publish (a b c)
    (setq org-export-with-toc t)
    (org-html-publish-to-html a b c)
    (setq org-export-with-toc nil)
    (org-ascii-publish-to-ascii a b c)
    (org-gfm-publish-to-gfm a b c))
  (setq org-directory my-org-dir
        org-startup-indented t
        org-babel-min-lines-for-block-output 1
        org-startup-folded "showeverything"
        org-startup-with-inline-images t
        org-src-preserve-indentation t
        org-use-speed-commands t
        org-hide-emphasis-markers t
        org-export-with-section-numbers nil
        org-export-with-toc t
        org-export-with-date nil
        org-export-time-stamp-file nil
        org-export-with-email t
        org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate
        org-babel-default-header-args
          (cons '(:noweb . "yes")
                (assq-delete-all :noweb org-babel-default-header-args))
        org-babel-default-header-args
          (cons '(:exports . "both")
                (assq-delete-all :exports org-babel-default-header-args))
        org-babel-default-header-args
          (cons '(:results . "output verbatim replace")
                (assq-delete-all :results org-babel-default-header-args))
        org-ellipsis " ▼ "
        org-publish-project-alist
        `(("git"
           :base-directory "~/org/git"
           :publishing-directory ,my-org-publish-dir
           :publishing-function my-publish
           :completion-function (my-git-publish my-firefox))))
  (custom-set-faces '(org-ellipsis ((t (:foreground "gray40" :underline nil)))))
  (global-set-key (kbd "C-c c") 'org-capture)
  (global-set-key (kbd "C-c l") 'org-store-link)
  (global-set-key (kbd "C-c a") 'org-agenda)
  :bind (("M-p" . #'org-publish))
  :hook
  (after-save . my-after-save-hook)
  (org-mode . my-org-mode-hook))


(org-babel-do-load-languages
 'org-babel-load-languages
 '((shell . t)
   (python . t)
   (ruby . t)
   (perl . t)
   (emacs-lisp . t)
   (dot . t)
   (jupyter .t)))

(use-package helm
  :delight
  :bind (("M-x"     . #'helm-M-x))
  :bind (("C-x C-f" . #'helm-find-files))
  :bind (("C-x C-b" . #'helm-buffers-list))
  :config
  (use-package helm-flyspell :after (helm flyspell))
  (use-package helm-xref)
  (if (< emacs-major-version 27)
      (setq xref-show-xrefs-function 'helm-xref-show-xrefs)
    (setq xref-show-xrefs-function 'helm-xref-show-xrefs-27))
  (use-package helm-rg)
  (require 'helm-config)
  (use-package helm-projectile
    :init
    (setq helm-projectile-fuzzy-match nil)
    :config
    (helm-projectile-on))
  (helm-mode 1))

(use-package projectile
  :commands projectile-mode
  :init
  (add-hook 'after-init-hook 'projectile-mode)
  (setq projectile-indexing-method 'alien)
  (setq projectile-project-search-path '("~/org" "~/git"))
  (setq projectile-use-git-grep t)
  (setq projectile-tags-command "/usr/bin/ctags -Re -f \"%s\" %s")
  (setq projectile-mode-line-prefix " Proj")
  (setq projectile-completion-system 'helm)
  :config
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

(use-package magit
  :bind (("C-x g" . magit-status)))

(use-package git-gutter
  :defer t
  :diminish ""
  :init
  (add-hook 'prog-mode-hook 'git-gutter-mode)
  (add-hook 'org-mode-hook 'git-gutter-mode)
  :config
  (setq git-gutter:update-interval 2))

(use-package flycheck-yamllint
  :init
  (progn
    (eval-after-load 'flycheck
      '(add-hook 'flycheck-mode-hook 'flycheck-yamllint-setup))))

(use-package flycheck
  :init
  (setq flycheck-ruby-rubocop-executable "/Users/mikeh/bin/rubocop.sh")
  (setq flycheck-ruby-executable "/Users/mikeh/.rbenv/shims/ruby")
  (setq flycheck-emacs-lisp-load-path load-path)
  (add-hook 'after-init-hook 'global-flycheck-mode)
  :config
    (setq-default flycheck-disabled-checkers
		  (append flycheck-disabled-checkers
			  '(emacs-lisp-checkdoc)
			  '(ruby-rubylint)
			  '(yaml-ruby)
			  '(yaml-jsyaml)
			  '(json-jsonlint)
			  '(puppet-lint)
			  '(json-python-json))))

(use-package flycheck-golangci-lint
  :hook (go-mode . flycheck-golangci-lint-setup))

(use-package flyspell
  :commands flyspell-mode
  :config
  (setq ispell-program-name "aspell"
        ispell-extra-args '("--sug-mode=ultra"))
  (add-hook 'text-mode-hook #'flyspell-mode)
  (add-hook 'org-mode-hook #'flyspell-mode)
  (add-hook 'prog-mode-hook #'flyspell-prog-mode))

(use-package ruby-mode
  :config
  (defun my-ruby-mode-hook ()
    (add-hook 'before-save-hook 'delete-trailing-whitespace nil 'local)
    (setq ruby-insert-encoding-magic-comment nil))
  (add-hook 'ruby-mode-hook 'my-ruby-mode-hook))

(use-package yaml-mode
 :mode ("\\.yml$" . yaml-mode))

(use-package company
  :delight
  :init
  (global-company-mode)
  :config
  (define-key company-active-map (kbd "C-n") 'company-select-next-or-abort)
  (define-key company-active-map (kbd "C-p") 'company-select-previous-or-abort)
  (add-hook 'after-init-hook 'global-company-mode)
  ;; put most often used completions at stop of list
  (setq company-transformers '(company-sort-by-occurrence))
  (setq company-tooltip-limit 30)
  (setq company-idle-delay .3)
  (setq company-echo-delay 0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package company-terraform
  :config
  (add-to-list 'company-backends 'company-terraform))

(use-package yasnippet
  :config
  (yas-reload-all)
  (yas-global-mode))

(use-package yasnippet-snippets)

(use-package lsp-mode
  :hook
  ((go-mode ruby-mode lua-mode) . lsp)
  :commands (lsp lsp-deferred))

(use-package lsp-ui :commands lsp-ui-mode)
(use-package company-lsp
  :after lsp-mode
  :config
  (setq company-lsp-enable-recompletion t)
  :commands company-lsp)

(use-package helm-lsp :commands helm-lsp-workspace-symbol)
(use-package lsp-treemacs :commands lsp-treemacs-errors-list)

(use-package pyenv-mode
  :config
    (defun projectile-pyenv-mode-set ()
      "Set pyenv version matching project name."
      (let ((project (projectile-project-name)))
        (if (member project (pyenv-mode-versions))
            (pyenv-mode-set project)
          (pyenv-mode-unset))))

    (add-hook 'projectile-switch-project-hook 'projectile-pyenv-mode-set)
    (add-hook 'python-mode-hook 'pyenv-mode))

(use-package pyenv-mode-auto)

(use-package lsp-python-ms
  :demand
  :hook (python-mode . lsp))

(use-package python
  :mode ("\\.py\\'" . python-mode)
        ("\\.wsgi$" . python-mode)
  :interpreter ("python" . python-mode)

  :init
  (setq-default indent-tabs-mode nil)

  :config
  (setq python-indent-offset 4)
  (setq python-indent-guess-indent-offset-verbose nil))

(use-package jupyter)

;; allow remembering risky variables
;(setq enable-local-variables :all)
(defun risky-local-variable-p (sym &optional _ignored) nil)

(use-package go-mode
  :init
  :config
  (defun my-go-mode-hook ()

    (setq gofmt-command "goimports")
    (setq godoc-and-godef-command "go doc")
    (add-hook 'before-save-hook 'gofmt-before-save)
    (if (not (string-match "go" compile-command))
	(set (make-local-variable 'compile-command)
	     "go build -v"))
    (company-mode)
    (setq tab-width 4)
    (message "go-mode: Ya gone and did it now!"))
  (add-hook 'go-mode-hook 'my-go-mode-hook)
  (add-hook 'before-save-hook 'delete-trailing-whitespace))

(require 'company-lua)

(defun set-company-backends-for-lua()
  "Set lua company backend."
  (setq-local company-backends '(
                                 (
                                  company-lua
                                  company-keywords
                                  company-gtags
                                  company-yasnippet
                                  )
                                 company-capf
                                 company-dabbrev-code
                                 company-files
                                 )))

(use-package lua-mode
  :mode "\\.lua$"
  :interpreter "lua"
  :hook (lua-mode . set-company-backends-for-lua)
  :config
  (setq lua-indent-level 4))

(use-package lsp-lua-emmy
  :demand
  :ensure nil
  :load-path "~/git/lsp-lua-emmy"
  :hook (lua-mode . lsp)
  :config
  (setq lsp-lua-emmy-jar-path (expand-file-name "EmmyLua-LS-all.jar" user-emacs-directory))
  )

(use-package dot-mode)
