if [[ $TERM = dumb ]]; then
    export PS1='$ '
    return
fi

set -o physical

alias grep='ggrep --color=auto'
alias fgrep='gfgrep --color=auto'
alias egrep='gegrep --color=auto'
alias ls='gls --color=auto'
alias diff='colordiff'
alias grpe='ggrep'
alias gre='ggrep'
alias gpt='gpg'

export GOPATH=/Users/mikeh/go
export PATH=~/bin:$GOPATH/bin:$PATH
export PATH="/usr/local/MacGPG2/bin/gpg2:/usr/local/bin:/usr/local/sbin:$PATH"
export MANPATH="$MANPATH:/usr/local/opt/coreutils/libexec/gnuman"
export STOW_DIR=/Users/mikeh/git/dotfiles
export PYENV_VIRTUALENV_DISABLE_PROMPT=1

eval "$(gdircolors)"

alias controlkill='for sock in $(ls ~/.ssh/control); do ssh -O exit $sock; done'
alias controlcheck='for sock in $(ls ~/.ssh/control); do ssh -O check $sock; done'

shopt -s histappend
HISTCONTROL=ignoredups:erasedups
HISTSIZE=100000
CDPATH=~/.paths

export PS1='\h:\w\$ '

if [ -f "/usr/local/opt/bash-git-prompt/share/gitprompt.sh" ]; then
    GIT_PROMPT_ONLY_IN_REPO=1
    GIT_PROMPT_SHOW_UPSTREAM=1
    GIT_PROMPT_SHOW_UNTRACKED_FILES=all
    __GIT_PROMPT_DIR="/usr/local/opt/bash-git-prompt/share"
    source "/usr/local/opt/bash-git-prompt/share/gitprompt.sh"
fi

if [[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]]; then
    source "/usr/local/etc/profile.d/bash_completion.sh"
fi

loose()
{
    local OPTIND OPTARG findcmd start field dopretty
    dopretty=false
    while getopts "a" flag; do
	case $flag in
	    a) dopretty=true; shift;;
	    *) echo "Unknown flag."; return 1;
        esac
    done

    # This is if we find ourselves in a bare repo.
    if [ -d '.git/objects' ]; then
	start='.git/objects';
	field=3
    elif [ -d 'objects' ]; then
	start='objects';
	field=2
    fi

    findcmd="find $start -type f -exec /bin/ls -tr {} +"
    for f in $($findcmd | cut -d/ -f$field- | grep -Ev '^(pack|info)' | tr -d /); do
	local type size pretty
	type=$(git cat-file -t "$f");
	size=$(git cat-file -s "$f");
	if [ $dopretty = true ]; then
	    pretty=$(git cat-file -p "$f" | perl -pe 's/\n/\n\t/g');
	fi
	printf "%s: %s (%s)\n\t%s\r" "$f" "$type" "$size" "$pretty"
    done
}

dotgrab()
{
    if [ "$#" == "0" ]; then
	echo 'Gotta specify some dots to grab.'
	return 1;
    fi
    stow --target="$HOME" "$@"
}

_dotgrab_show()
{
    local hosts cur
    hosts=$(command gfind $STOW_DIR -maxdepth 1 -type d | gxargs -i basename {} | grep -vE "^(\.|dotfiles)")
    cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=( $(compgen -W "$hosts" "$cur") )
}
complete -F _dotgrab_show dotgrab

emacs()
{
    if [ "$#" == "0" ]; then
	emacsclient -a= -t
    else
	emacsclient -a= -t "$@"
    fi
}

gmacs()
{
    if [ "$#" == "0" ]; then
	emacsclient -a= -nc
    else
	emacsclient -a= -nc "$@"
    fi
}

goto()
{
    local OPTIND OPTARG
    local TMUXCMD='LANG="en_US.UTF-8" tmux set-option -g prefix C-o \; new-session -A -D -s'

    while getopts "n" flag; do
	case "$flag" in
	    n) TMUXCMD=""; shift;;
	    *) echo "Unknown flag."; return 1;
	esac
    done

    if [ "$#" == "0" ]; then
	echo 'Need someplace to go.'
	return 1;
    fi

    while (( "$#" )); do
	tmux new-window -n "$1" ssh -tt -q "$1" "$TMUXCMD $1"
	shift
    done
}

_goto_show()
{
    local cur hosts

    hosts=$(command perl -ne 'next unless /^host\s+(.*)/;print"$1\n" unless $1 eq "*";' ~/.ssh/config)
    cur="${COMP_WORDS[COMP_CWORD]}"
    COMPREPLY=( $(compgen -W "$hosts" "$cur") )
}
complete -F _goto_show goto
