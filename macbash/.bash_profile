HISTFILESIZE=100000
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
fi

if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi

if command -v rbenv 1>/dev/null 2>&1; then
    eval "$(rbenv init -)"
fi

if command -v rbenv 1>/dev/null 2>&1; then
    eval "$(plenv init -)"
fi
